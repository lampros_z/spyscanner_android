package gr.hua.ictapps.android.spyscanner;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * This map activity, reads all the geographical coordinates that have been stored to the database and places a marker on each and every one of them.
 * On every marker, a tag is placed showing the lat, lon coordinates.
 *
 */
public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GeoLocationsContract {

    private GoogleMap mMap;
    final Uri uri = Uri.parse(GeoLocationsContract.GEOLOCATIONS_URL);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        ContentResolver contentResolver = getContentResolver();
        Cursor cursor = contentResolver.query(uri, null, null, null, null, null);
        List<GeographicLocation> gls = new ArrayList<>();
        if(cursor.moveToFirst()){
            do{
                gls.add(new GeographicLocation(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3)));
            }while (cursor.moveToNext());
        }
        if(gls!= null){
            for(GeographicLocation gl : gls){
                mMap.addMarker(new MarkerOptions().position(new LatLng(gl.getLatitude(), gl.getLongitude())).title("lat: " + gl.getLatitude()+" | lon: "+ gl.getLongitude()));
            }
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(gls.get(gls.size()-1).getLatitude(), gls.get(gls.size()-1).getLongitude())));
        }else {
            // Add a marker in Sydney and move the camera
            LatLng sydney = new LatLng(-34, 151);
            mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        }
    }
}
