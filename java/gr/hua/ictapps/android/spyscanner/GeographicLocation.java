package gr.hua.ictapps.android.spyscanner;

/**
 * GeographicLocation class: Holds information about geographical coordinates(lat, lon).
 * A unix timestamp is also used to track the time period that the coordinates inserted to the database.
 */
public class GeographicLocation {

    private int id;
    private String timestamp;
    private double longitude;
    private double latitude;

    public GeographicLocation(int id, String timestamp, double latitude, double longitude) {
        this.id = id;
        this.timestamp = timestamp;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
