package gr.hua.ictapps.android.spyscanner;

import android.content.ContentResolver;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * This class has a view with a data table that shows to the user all the geographical coordinates that stored in the database.
 * !!What is important about this class, is that, it uses a Timer to fire an async task every 1 second in order to update the contents of the data table.
 * In other words, this class provides the functionality for real-time data presentation. A user can walk with his phone an see every second a new value appended to the data table.
 */
public class ResultsActivity extends AppCompatActivity implements GeoLocationsContract {

    final Uri uri = Uri.parse(GeoLocationsContract.GEOLOCATIONS_URL);
    Timer timer = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
            final Handler handler = new Handler();
            Timer timer = new Timer();
           TimerTask task = new TimerTask() {
                @Override
                public void run() {
                    handler.post(new Runnable() {
                        public void run() {
                            new AsyncWorker().execute(0);
                        }
                    });
                }
            };
            timer.schedule(task, 0, 1000); //it executes this every 1000ms
    }

    public class AsyncWorker extends AsyncTask<Integer, List<GeographicLocation>, String>{

        @Override
        protected String doInBackground(Integer... integers) {
            ContentResolver contentResolver = getContentResolver();
            Cursor cursor = contentResolver.query(uri, null, null, null, null, null);
            List<GeographicLocation> gls = new ArrayList<>();
            if(cursor.moveToFirst()){
                do{
                    gls.add(new GeographicLocation(cursor.getInt(0), cursor.getString(1), cursor.getDouble(2), cursor.getDouble(3)));
                }while (cursor.moveToNext());
            }

            publishProgress(gls);
            return "DONE";
        }

        @Override
        protected void onProgressUpdate(List<GeographicLocation>... values) {
            super.onProgressUpdate(values);
            List<GeographicLocation> gls;
            gls = values[0];
            TableLayout tableLayout = findViewById(R.id.data_table);
            tableLayout.removeAllViews();

            TableRow tr_header = new TableRow(ResultsActivity.this);
            tr_header.setBackgroundColor(Color.parseColor("#ff5722"));

            TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.0f);


            TextView label_id = new TextView(ResultsActivity.this);
            label_id.setText("id");
            label_id.setTextColor(Color.WHITE);
            label_id.setTextSize(TypedValue.COMPLEX_UNIT_DIP,13.5f);
            label_id.setGravity(Gravity.CENTER);
            label_id.setLayoutParams(layoutParams);
            tr_header.addView(label_id);


            TextView label_timestamp = new TextView(ResultsActivity.this);
            label_timestamp.setText("timestamp");
            label_timestamp.setTextSize(TypedValue.COMPLEX_UNIT_DIP,13.5f);
            label_timestamp.setTextColor(Color.WHITE);
            label_timestamp.setGravity(Gravity.CENTER);
            label_timestamp.setLayoutParams(layoutParams);
            tr_header.addView(label_timestamp);


            TextView label_latitude = new TextView(ResultsActivity.this);
            label_latitude.setText("latitiude");
            label_latitude.setTextSize(TypedValue.COMPLEX_UNIT_DIP,13.5f);
            label_latitude.setTextColor(Color.WHITE);
            label_latitude.setGravity(Gravity.CENTER);
            label_latitude.setLayoutParams(layoutParams);
            tr_header.addView(label_latitude);


            TextView label_longitude = new TextView(ResultsActivity.this);
            label_longitude.setText("longitude");
            label_longitude.setTextSize(TypedValue.COMPLEX_UNIT_DIP,13.5f);
            label_longitude.setTextColor(Color.WHITE);
            label_longitude.setGravity(Gravity.CENTER);
            label_longitude.setLayoutParams(layoutParams);
            tr_header.addView(label_longitude);

            tableLayout.addView(tr_header, new TableLayout.LayoutParams(
                    TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT));

            TextView[] idTextArray = new TextView[gls.size()];
            TextView[] timestampTextArray = new TextView[gls.size()];
            TextView[] latitudeTextArray = new TextView[gls.size()];
            TextView[] longitudeTextArray = new TextView[gls.size()];
            TableRow[] tr_head = new TableRow[gls.size()];


            for (int i = 0; i < gls.size(); i++) {
                tr_head[i] = new TableRow(ResultsActivity.this);
                tr_head[i].setBackgroundColor(Color.parseColor("#303841"));

                TableRow.LayoutParams idLayoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.0f);
                idLayoutParams.setMargins(10,0,10,10);
                idTextArray[i] = new TextView(ResultsActivity.this);
                idTextArray[i].setText(Integer.toString(gls.get(i).getId()));
                idTextArray[i].setGravity(Gravity.CENTER);
                idTextArray[i].setLayoutParams(idLayoutParams);
                idTextArray[i].setTextColor(Color.WHITE);

                TableRow.LayoutParams timestampLayoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.0f);
                timestampLayoutParams.setMargins(0,0,10,10);
                timestampTextArray[i] = new TextView(ResultsActivity.this);
                timestampTextArray[i].setTextColor(Color.WHITE);
                timestampTextArray[i].setGravity(Gravity.CENTER);
                timestampTextArray[i].setLayoutParams(layoutParams);
                timestampTextArray[i].setText(gls.get(i).getTimestamp());

                TableRow.LayoutParams latitudeLayoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.0f);
                latitudeLayoutParams.setMargins(0,0,10,10);
                latitudeTextArray[i] = new TextView(ResultsActivity.this);
                latitudeTextArray[i].setTextColor(Color.WHITE);
                latitudeTextArray[i].setGravity(Gravity.CENTER);
                latitudeTextArray[i].setLayoutParams(layoutParams);
                latitudeTextArray[i].setText(Double.toString(gls.get(i).getLatitude()));

                TableRow.LayoutParams longitudeLayoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1.0f);
                longitudeLayoutParams.setMargins(0,0,10,10);
                longitudeTextArray[i] = new TextView(ResultsActivity.this);
                longitudeTextArray[i].setText(Double.toString(gls.get(i).getLongitude()));
                longitudeTextArray[i].setTextColor(Color.WHITE);
                longitudeTextArray[i].setGravity(Gravity.CENTER);
                longitudeTextArray[i].setLayoutParams(layoutParams);

                tr_head[i].addView(idTextArray[i]);
                tr_head[i].addView(timestampTextArray[i]);
                tr_head[i].addView(latitudeTextArray[i]);
                tr_head[i].addView(longitudeTextArray[i]);

                tableLayout.addView(tr_head[i]);

            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onStop() {
        if( timer != null) {
            timer.cancel();
            timer.purge();
        }
        super.onStop();
    }
}
