package gr.hua.ictapps.android.spyscanner;

import android.Manifest;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

/**
 * On the onCreate method of this class a broadcast receiver is registered dynamically on the runtime to listen for network change events.
 */
public class MainActivity extends AppCompatActivity {
    private NetworkStateReceiver networkStateReceiver = null;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&  checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
            return;
        }

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        networkStateReceiver = new NetworkStateReceiver();
        registerReceiver(networkStateReceiver, filter);

        Button button = findViewById(R.id.view_table);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClassName("gr.hua.ictapps.android.spyscanner", "gr.hua.ictapps.android.spyscanner.ResultsActivity");
                startActivity(intent);
            }
        });

        Button mapBtn = findViewById(R.id.map_btn);
        mapBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClassName("gr.hua.ictapps.android.spyscanner", "gr.hua.ictapps.android.spyscanner.MapsActivity");
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (networkStateReceiver != null)
            unregisterReceiver(networkStateReceiver);
        super.onDestroy();
    }
}
