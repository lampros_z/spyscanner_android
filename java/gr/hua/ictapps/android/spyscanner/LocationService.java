package gr.hua.ictapps.android.spyscanner;

import android.Manifest;
import android.app.Service;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

/**
 * A service that tracks the location of the user.
 * If the user moved 10 meters away from the previous location within a time period of 5 seconds a record containing geographical coordinates is inserted to the database via the content resolver.
 */
public class LocationService extends Service implements GeoLocationsContract{
    final Uri uri = Uri.parse(GeoLocationsContract.GEOLOCATIONS_URL);
    private ContentValues contentValues;
    private ContentResolver contentResolver;
    private LocationManager locationManager = null;
    private LocationListener locationListener = null;
    private HandlerThread handlerThread;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onCreate() {
        super.onCreate();
        contentResolver = getContentResolver();
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&  checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                contentValues = new ContentValues();
                contentValues.put(GeoLocationsContract.KEY_LAT, location.getLatitude());
                contentValues.put(GeoLocationsContract.KEY_LON, location.getLongitude());
                contentResolver.insert(uri, contentValues);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
        handlerThread = new HandlerThread("LocationListenerThread");
        handlerThread.start();
        /*The callbacks of onLocationChanged() will be served by the HandlerThread*/
        Looper looper = handlerThread.getLooper();
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 10, locationListener, looper);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000, 10, locationListener, looper);
    }


    @Override
    public void onDestroy() {
        if(locationManager != null && locationListener != null) {
            locationManager.removeUpdates(locationListener);
        }
        super.onDestroy();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
