package gr.hua.ictapps.android.spyscanner;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.widget.Toast;


public class NetworkStateReceiver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {
        /*Check whether there is internet connection connection available via cellular data or wifi.*/
        /*If network connection is available, this broadcast receiver starts a service that will insert geographical coordinates to database based on the users location*/
        /*If there is not network connection available then the service is stopped*/
        final ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        final android.net.NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        final android.net.NetworkInfo mobile = connMgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi.isConnected() || mobile.isConnected()) {
            Toast.makeText(context, "Connected", Toast.LENGTH_SHORT).show();
            Intent intent1 = new Intent();
            intent1.setClassName("gr.hua.ictapps.android.spyscanner", "gr.hua.ictapps.android.spyscanner.LocationService");
            context.startService(intent1);
        } else {
            Toast.makeText(context, "Disconected", Toast.LENGTH_SHORT).show();
            Intent intent1 = new Intent();
            intent1.setClassName("gr.hua.ictapps.android.spyscanner", "gr.hua.ictapps.android.spyscanner.LocationService");
            context.stopService(intent1);
        }

    }

}
