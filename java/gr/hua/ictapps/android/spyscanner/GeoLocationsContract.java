package gr.hua.ictapps.android.spyscanner;

/**
 * Contract class(interface) to provide all necessary information needed when communication with the data layer is needed.
 */
public interface GeoLocationsContract {

    String DB_NAME = "GEO_DB";
    int DB_VERSION = 1;
    String tableName = "GEOGRAPHIC_LOCATIONS";
    String KEY_ID = "id";
    String KEY_UNIX_TIMESTAMP = "unix_timestamp";
    String KEY_LAT = "lat";
    String KEY_LON = "lon";
    String AUTHORITY = "gr.hua.ictapps.android.spyscanner";

    String GEOLOCATIONS_URL = "content://gr.hua.ictapps.android.spyscanner/geolocations";

}
