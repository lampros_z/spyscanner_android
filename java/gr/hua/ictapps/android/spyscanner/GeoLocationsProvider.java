package gr.hua.ictapps.android.spyscanner;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * GeoLocationsProvider exposes an api to the data layer of this application.
 */
public class GeoLocationsProvider extends ContentProvider implements GeoLocationsContract {

    private UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);


    @Override
    public boolean onCreate() {
        matcher.addURI(GeoLocationsContract.AUTHORITY, "geolocations", 1);
        return false;
    }



    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] strings, @Nullable String s, @Nullable String[] strings1, @Nullable String s1) {
        if(matcher.match(uri) == 1){
            GeoLocationsOpenHelper geoLocationsOpenHelper = new GeoLocationsOpenHelper(getContext());
            return geoLocationsOpenHelper.getReadableDatabase().query(GeoLocationsContract.tableName, null, null , null, null, null, null);
        }
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        GeoLocationsOpenHelper geoLocationsOpenHelper = new GeoLocationsOpenHelper(getContext());

        long id = geoLocationsOpenHelper.getWritableDatabase().insert(GeoLocationsContract.tableName,null,contentValues);
        return Uri.parse(uri.toString() + "/" + id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        return 0;
    }


}
