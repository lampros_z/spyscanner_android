package gr.hua.ictapps.android.spyscanner;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

/**
 * A helper class managing connection with the sqlite database providing two methods to the content provider class.
 */
public class GeoLocationsOpenHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "GEO_DB";
    private static final int DB_VERSION = 1;

    private static final String tableName = "GEOGRAPHIC_LOCATIONS";

    private static final String KEY_ID = "id";
    private static final String KEY_UNIX_TIMESTAMP = "unix_timestamp";
    private static final String KEY_LAT = "lat";
    private static final String KEY_LON = "lon";

    private static final String CREATE_QUERY = "create table " + tableName + " ("
            + KEY_ID + " integer primary key autoincrement, "
            + KEY_UNIX_TIMESTAMP + " timestamp default (strftime('%s', 'now')), "
            + KEY_LAT + " double, "
            + KEY_LON + " double);";

    public GeoLocationsOpenHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_QUERY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    /**
        *Insert longitude and latitude to database.
        *
        *@param  contentValues Contains the geographical coordinates values.
     */
    public long insertGeoLocation(ContentValues contentValues){

        return getWritableDatabase().insert(tableName, null, contentValues);
    }

    /**
      *Return all records from the database.
     */
    public Cursor getAllGeolocations(){
        return getReadableDatabase().query(tableName,
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );
    }


}
